<?php

namespace Whoisdoma\API\Http\Controllers;

use Whoisdoma\DomainParser\Parser as DomainParser;
use Whoisdoma\WhoisParser\Parser as WhoisParser;
use Whoisdoma\API\Helpers\ApiData;

class DomainWhois extends Controller {

    private $apidata;

    public function __construct()
    {
        $this->apidata = new ApiData();
    }

    public function lookupDomain(){

        if (!$this->apidata->checkForDomain()) {
            return response()->json(['success' => 'false', 'result' => 'No domain provided.'], 500);
        }

        $parser = new WhoisParser();
        $parser->setFormat('json');

        return $parser->lookup($this->apidata->getDomain());
    }

}
