<?php

namespace Whoisdoma\API\Helpers;

//use Whoisdoma\API\Database\Models\ApiKey;
use Illuminate\Http\Request;

class ApiData {

    public $apikey, $domain, $ip_address, $whois_server;

    public function checkForApiKey() {

        $this->apikey = request()->get('api_key');

        if (!$this->apikey) {
            return false;
        } else {
            return true;
        }

    }

    /*public function verifyApiKey() {
        //verify the api key provided
        $checkApiKey = ApiKey::where('key', '=', $this->apikey)->count();
        if ($checkApiKey == 0) {
            return false;
        } else {
            return true;
        }
    }*/

    public function checkForDomain() {
        //verify that there is a domain provided
        $this->domain = request()->get('domain');
        if (!$this->domain) {
            return false;
        } else {
            return true;
        }
    }

    public function checkForIPAddress() {
        //verify that an ip address was provided
        $this->ip_address = request()->get('ip');
        if (!$this->ip_address) {
            return false;
        } else {
            return true;
        }
    }

    /*public function checkApiKey() {

        $this->apikey = request()->input('api_key');

        //see if we have an api key
        if (!$this->apikey) {

            //no api key provided
            return response()->json(['success' => false, 'result' => 'No api key provided.'], 400);

        } else {

            //verify the api key provided
            $checkApiKey = ApiKey::where('key', '=', $this->apikey)->count();
            if ($checkApiKey == 0) {

                //api key is not valid
                return response()->json(['success' => false, 'result' => 'The api key provided could not be found or is not valid.'], 400);
            }

        }

    }*/

    public function getDomain() {
        return $this->domain;
    }

}